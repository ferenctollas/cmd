Cmd class provides a solution to easy implement command line applications.

# Create implementation #
* extend Cmd class
* add commands. 

# Example #
In this example, we need to implement a command named "Start". 
First add a method named "**cmdStart(String parameters)**".

When the user enters the Start command in command line, the cmdStart method will be called with the available parameters.

If a help must be provided for the Start command, a new method must be added : "**helpStart(String parameters)**"

When the user enters "help Start" then this helpStart method will be run.
Command names are case sensitive.

# Get all available commands #
Enter "help" command in the console

# Change the prompt #
From the class which extends the Cmd class, call the setPrompt method. The prompt can also be changed in the constructor.

# Example code #


```
#!java
public final class CmdExample extends Cmd {

    public CmdExample() {
        super();
    }

    public void cmdStart(final String parameter) {
        this.print(String.format("cmdStart has been called with parameter %s", parameter));
    }

    public void helpStart(final String parameter) {
        this.print("Help for the Start command");
    }

    public void cmdExit(final String parameter) {
        this.terminateLoop();
    }

    public static void main(String[] args) {
        new CmdExample().loop();
    }
}


```