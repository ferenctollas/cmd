package mxr.cmd;

import java.io.Console;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Set;

/**
 * Created by mxrtools on 01/02/15.
 */
public class Cmd {
    private String prompt = "> ";
    private HashMap<String, Method> commands = null;
    private HashMap<String, Method> helps = null;
    private static final String CMD = "cmd";
    private static final String HELP = "help";
    private boolean terminate = false;


    protected Cmd() {
        this.commands = new HashMap<String, Method>();
        this.helps = new HashMap<String, Method>();
        this.discover();
    }

    protected Cmd(final String prompt) {
        this();
        this.prompt = prompt;
    }

    protected void setPrompt(final String prompt) {
        this.prompt = prompt;
    }

    public void print(final String message) {
        System.console().writer().write(message);
        System.console().writer().write("\n");
        System.console().writer().flush();
    }

    private void discover() {
        Method[] methods = this.getClass().getMethods();
        for (Method method : methods) {
            String _methodName = method.getName();
            if (_methodName.startsWith(Cmd.CMD)) {
                this.commands.put(getAction(_methodName, Cmd.CMD), method);
            } else if (_methodName.startsWith(Cmd.HELP)) {
                this.helps.put(this.getAction(_methodName, Cmd.HELP), method);
            }
        }
    }

    protected void terminateLoop() {
        this.terminate = true;
    }

    private String getAction(final String command, final String type) {
        if (type.equals(Cmd.CMD)) {
            return command.substring(Cmd.CMD.length(), command.length());
        } else if (type.equals(Cmd.HELP)) {
            return command.substring(Cmd.HELP.length(), command.length());
        }
        return null;
    }

    private void doCallAction(final String userCommand) {
        if (userCommand.trim().isEmpty()) {
            return;
        }
        String[] parts = userCommand.split(" ");
        if (parts[0].equalsIgnoreCase("help")) {
            if (parts.length == 1) {
                // print available helps
                Set<String> _commands = this.commands.keySet();
                this.print("Available commands:");
                for (String cmd : _commands) {
                    this.print(cmd);
                }
            } else
                this.doHelp(parts[1], userCommand);
        } else {
            if (!this.commands.containsKey(parts[0])) {
                this.print("Command not found!");
            } else {
                Method _m = this.commands.get(parts[0]);
                this.invoke(_m, userCommand.substring(parts[0].length(), userCommand.length()));
            }
        }
    }

    private void invoke(final Method method, final String parameter) {
        try {
            method.invoke(this, parameter);
        } catch (IllegalAccessException e) {
            this.print("Cannot call command : " + e.getLocalizedMessage());
        } catch (InvocationTargetException e) {
            this.print("Cannot call command : " + e.getLocalizedMessage());
        }
    }

    private void doHelp(final String command, final String userCommand) {
        if (!this.helps.containsKey(command)) {
            this.print("No help for command");
        } else {
            Method _m = this.helps.get(command);
            this.invoke(_m, command);
        }
    }

    public Set<String> getAvailableCommands() {
        return this.commands.keySet();
    }

    public void loop() {
        Console console = System.console();
        PrintWriter pw = console.writer();
        while (!this.terminate) {
            pw.write(this.prompt);
            pw.flush();
            this.doCallAction(console.readLine());
        }
    }
}
