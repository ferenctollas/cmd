package mxr.example;

import mxr.cmd.Cmd;

/**
 * Created by mxrtools on 01/02/15.
 */
public final class CmdExample extends Cmd {

    public CmdExample() {
        super();
    }

    public void cmdStart(final String parameter) {
        this.print(String.format("cmdStart has been called with parameter %s", parameter));
    }

    public void helpStart(final String parameter) {
        this.print("Help for the Start command");
    }

    public void cmdExit(final String parameter) {
        this.terminateLoop();
    }

    public static void main(String[] args) {
        new CmdExample().loop();
    }
}
